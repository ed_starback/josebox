// This #include statement was automatically added by the Particle IDE.
#include <adafruit-led-backpack.h>

// -----------------------------------
// Controlling LEDs over the Internet
// -----------------------------------

int MAX = 255;
float MIDDLE = 50.0;

int yearMultiplier = 4;
const int maxYearDisplay = 301;
const int maxYear = maxYearDisplay * yearMultiplier;

int year = 0;
int yearDisplay = 0;
int oldYear = -1;   // If the year changes recalculate the rgb values

int chargeRate = 1;
int dischargeRate = 1;
int chargeLevel[maxYearDisplay];
int oldChargeLevel = -1;

int dischargedLevel = 0;

int counter = 0;
int counterMax = 10;

int ledRed = D3;
int redOn = 0;
int ledGreen = TX;
int greenOn = 0;

// For Arduino UNO, only pins 2 & 3 work for interrupts
int encoderDialPin1 = D2;
int encoderDialPin2 = D4;

volatile int changeInTicks = 0;

// Don't need to be volatile since only accessed from single interrupt handler
int _lastBit1 = 0;
int _lastBit2 = 0;

int powerButton = D6;
int dischargeButton = D5;

bool enabled = true;
bool charging = false;
bool discharging = false;

int val = 0;

Adafruit_7segment matrix = Adafruit_7segment();

void setup()
{
    #ifndef __AVR_ATtiny85__
        Serial.begin(9600);
        Serial.println("7 Segment Backpack Test");
    #endif
    matrix.begin(0x70);
  
    pinMode(ledRed, OUTPUT);
    pinMode(ledGreen, OUTPUT);

    pinMode(powerButton, INPUT);
    pinMode(dischargeButton, INPUT);

    // Encoder
    pinMode(encoderDialPin1, INPUT); 
    pinMode(encoderDialPin2, INPUT);
  
    // Turn on pullup resistor
    digitalWrite(encoderDialPin1, HIGH);
    digitalWrite(encoderDialPin2, HIGH);
    
    attachInterrupt(0, handleEncoderChange, CHANGE); 
    attachInterrupt(1, handleEncoderChange, CHANGE);

    // declare cloud functions
    Particle.function("led", ledToggle);
    Particle.function("setRed", SetRed);
    Particle.function("setGreen", SetGreen);
    Particle.function("getYear", getYear);
    Particle.function("setYear", setYear);
    Particle.function("enable", enable);
    Particle.function("disable", disable);
    Particle.function("getEnabled", GetEnabled);
    Particle.function("charge", Charge);
    Particle.function("discharge", Discharge);
    Particle.function("stopCharging", StopCharging);
    Particle.function("getDchrgdLvl", GetDischargedLevel);
    Particle.function("setDchrgdLvl", SetDischargedLevel);
    Particle.function("setCharge", SetCharge);
    Particle.function("getCharge", GetCharge);
    Particle.function("getRed", GetRedCharge);
    Particle.function("getGreen", GetGreenCharge);

    Particle.function("getTicks", GetEncoderTicks);
    Particle.function("getYearRaw", GetYearRaw);
    Particle.function("getYearRMax", GetYearRawMax);

    Particle.function("getDimDelay", GetDimmingDelay);
    Particle.function("setDimDelay", SetDimmingDelay);

    for (int i = 0; i < maxYearDisplay; ++i)
    {
        chargeLevel[i] = 100;
    }
}

// have an outer loop for dio
// have an inner loop for changing led chargeLevel
void loop()
{
    // wait for a period to see the dimming effect
    delay(1);

    noInterrupts();
    year = year + changeInTicks;
    if (year < 0) {
        year = 0;
    }
    else if (year >= maxYear) {
        year = maxYear - 1;
    }

    yearDisplay = year / yearMultiplier;

    changeInTicks = 0;
    interrupts();
    
    val = digitalRead(powerButton);
    if (val) {
        enabled = true;
    }
    else {
        enabled = false;
    }
    
    val = digitalRead(dischargeButton);
    if (val) {
        discharging = true;
    }

    counter += 1;

    if (counter > counterMax)
    {
        if (charging) {
            chargeLevel[yearDisplay] = chargeLevel[yearDisplay] + chargeRate;
            if (chargeLevel[yearDisplay] >= 100) {
                chargeLevel[yearDisplay] = 100;
                charging = false;
            }
        }
    
        if (discharging) {
            chargeLevel[yearDisplay] = chargeLevel[yearDisplay] - dischargeRate;
            if (chargeLevel[yearDisplay] <= dischargedLevel) {
                chargeLevel[yearDisplay] = dischargedLevel;
                discharging = false;
                dischargedLevel = 0;
            }
        }
    
        counter = 0;
    }

    if (enabled == false) {
        analogWrite(ledRed, 0);
        analogWrite(ledGreen, 0);
        
        matrix.clear();
        matrix.writeDisplay();

        return;
    }
    else 
    {
        if (chargeLevel[yearDisplay] != oldChargeLevel || yearDisplay != oldYear)
        {
            CalculateLedBrightness();
            
            oldChargeLevel = chargeLevel[yearDisplay];
            oldYear = yearDisplay;
        }
        
        analogWrite(ledRed, redOn);
        analogWrite(ledGreen, greenOn);
    }

    matrix.println(yearDisplay);
    matrix.writeDisplay();
}

void handleEncoderChange() {
    int bit1 = digitalRead(encoderDialPin1);
    int bit2 = digitalRead(encoderDialPin2);
  
    int code = (_lastBit1 << 3) | (_lastBit2 << 2) | (bit1 << 1) | bit2;
    
    if (code == 0b0001 || code == 0b0111 || code == 0b1110 || code == 0b1000) {
      changeInTicks++;
    } else if (code == 0b1011 || code == 0b1101 || code == 0b0100 || code == 0b0010) {
      changeInTicks--;
    } else {
      // For this case, the direction of the turn is indeterminate
    }
    
    _lastBit1 = bit1;
    _lastBit2 = bit2;
  }

int CalculateLedBrightness() {
    if (chargeLevel[yearDisplay] > MIDDLE) {
        redOn = MAX * (1.0 - (chargeLevel[yearDisplay] - MIDDLE)/MIDDLE);
        greenOn = MAX;
    }
    else {
        redOn = MAX;
        greenOn = MAX * chargeLevel[yearDisplay]/MIDDLE;
    }
}

int GetDimmingDelay(String command) {
    return counterMax;
}

int SetDimmingDelay(String command) {
    counterMax = command.toInt();

    return 0;
}

int Charge(String command) {
    charging = true;
    discharging = false;

    return 0;
}

int Discharge(String command) {
    charging = false;
    discharging = true;

    return 0;
}

int StopCharging(String command) {
    charging = false;
    discharging = false;

    return 0;
}

int GetDischargedLevel(String command) {
    return dischargedLevel;
}

int SetDischargedLevel(String command) {
    dischargedLevel = command.toInt();

    return 0;
}

int SetCharge(String command) {
    chargeLevel[yearDisplay] = command.toInt();

    return 0;
}

int GetCharge(String command) {
    return chargeLevel[yearDisplay];
}

int getYear(String command) {
    return yearDisplay;
}

int GetYearRaw(String command) {
    return year;
}

int GetYearRawMax(String command) {
    return maxYear;
}

int setYear(String command) {
    yearDisplay = command.toInt();
    year = yearDisplay * yearMultiplier;


    return 0;
}

int enable(String command) {
    enabled = true;
    
    return 0;
}

int disable(String command) {
    enabled = false;
    
    return 0;
}

int GetEnabled(String command) {
    if (enabled) {
        return 1;
    }

    return 0;
}

// Debugging commands

int ledToggle(String command) {
    if (command == "redOn") {
        redOn = MAX;
        return 1;
    }
    else if (command == "redOff") {
        redOn = 0;
        return 0;
    }
    else if (command == "greenOn") {
        greenOn = MAX;
        return 1;
    }
    else if (command == "greenOff") {
        greenOn = 0;
        return 0;
    }
    else if (command == "allOn") {
        redOn = MAX;
        greenOn = MAX;
        return 1;
    }
    else if (command == "allOff") {
        redOn = 0;
        greenOn = 0;
        return 0;
    }

    return -1;
}

int SetRed(String command) {
    redOn = command.toInt();

    return 0;
}

int SetGreen(String command) {
    greenOn = command.toInt();

    return 0;
}

int GetRedCharge(String command) {
    return redOn;
}

int GetGreenCharge(String command) {
    return greenOn;
}


int GetEncoderTicks(String command) {
    return changeInTicks;
}