### Required
| Manufacturer | Name                                                                     | Part Number | Price | Qty  | Purchase       |
| ------------ | ------------------------------------------------------------------------ | ----------- | ----- | ---- | -------------- |
|              | Battery holder                                                           |             |       | 1    | Y              |
|              | Connecter to battery   holder                                              |             |       | 2    | Y              |
|              | On off switch for power - mountable so can be switch off from the inside |             |       |      |                |
| Adafruit     | UBEC DC/DC Step-Down (Buck) Converter - 5V @ 3A                          | 1385        | $9.95 | 2    | Y - equivalent |
|              | Transisters to allow board to control higher voltage for LEDs            |             |       |      | Y              |
|              | Brighter LEDs                                                            |             |       |      |                |



### Optional
| Manufacturer | Name                                                   | Part Number   | Price | Qty  |
| ------------ | --------------------------------                       | -----------   | ----- | ---- |
| Adafruit     | RGB Smart NeoPixel                                     | 1312          | 7.95  |      |
| Adafruit     | 8mm Chrome Metal LED Holder                            | 2173          | 5.95  |      |
| Adafruit     | 8mm Black Plastic LED Holder                           | 2172          | 1.50  |      |
| Adafruit     | Rugged Metal On/Off Switch with Green LED Ring - 16mm  | 482           | 4.95  |      |
| Adafruit     | LED Illuminated Pushbutton - 30mm Square               | 491           | 3.95  |      |
| Adafruit     | 16mm Illuminated Pushbutton - Blue Momentary           | 1477          | 1.95  |      |
| Adafruit     | Rugged Metal Pushbutton - 16mm 6V RGB Momentary        | 3350          | 10.95 |      |
| Adafruit     | Rugged Metal Pushbutton - 19mm 6V RGB Momentary        | 3425          | 14.95 |      |
| Adafruit     | Rugged Metal On/Off Switch - 22mm 6V RGB On/Off        | 3424          | 16.95 |      |
| Adafruit     | Panel Temperature Meter                                | 576           | 9.95  |      |



### Me
| Manufacturer | Name                                | Part Number | Price  | Qty  |
| ------------ | ----------------------------------- | ----------- | ------ | ---- |
| Adafruit     | USB Voltage Meter with OLED Display | 2690        | $19.95 | 1    |
| Adafruit     | Panel Current Meter                 | 574         | 9.95   | 1    |
| Adafruit     | Mini 2 wire Volt Meter              | 460         | 7.95   | 1    |
| Adafruit     | Panel Temperature Meter             | 576         | 9.95   | 1    |
| Adafruit     | 5V 1.5A Linear Voltage Regulator    | 2164        | 0.75   | 4    |
